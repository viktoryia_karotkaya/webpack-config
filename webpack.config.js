const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const dotenv = require('dotenv').config();

const config = {
  entry: path.resolve(__dirname, './src/index.js'),
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
          
          }
        }
      },
      {
        test: /\.css$/i,
        use: [
          {
            loader: 'style-loader'
          }, {
            loader: 'css-loader'
          }
        ]
      },
      {
        test: /\.svg$/,
        type: 'asset',
        use: 'svg-loader'
      }
    ]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'bundle.js',
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "src", "index.html"),
    })
    ]
}

module.exports = (env) => {
  console.log(env);
  if (env.development) {
    config.devtool = "inline-source-map";
    config.mode = "development";
    config.devServer = {
      open: true,
    };
  }
  
  if (env.production) {
    config.mode = "production";
    config.optimization = {
      minimize: true
    }
  }
  
  return config;
};

